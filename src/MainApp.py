import random,sys
import HangMe, FileFunctions, WordCategory

class Processor():
    def __init__(self):
        self.secretWord = ""
        self.progressWord = ""
    def createAsterix(self):
        # turn the secretWord into a bunch of Asterix
        for i in range(len(self.secretWord)):
            self.progressWord = self.progressWord + "*"
        # now turn progressWord into a list
        self.progressWord = list(self.progressWord)
    def findCharOccurence(self,myChar):
        #This is to ensure that progressWord is always a list
        self.progressWord = list(self.progressWord)
        #Look for the character
        if myChar in self.secretWord:
            for i in range(len(self.secretWord)-1):
                self.charPos = self.secretWord.find(myChar,i)
                if(self.charPos >= 0):
                    self.progressWord[self.charPos] = myChar
            return True
        else:
            return False        # no such letter in secretWord
    def list2String(self):
        self.progressWord = "".join(self.progressWord)

def createRandomWord(wordType):
    if(wordType == 1):
        # This function reads the dictionary file and randomly picks a word to return
        resultset = FileFunctions.FileHandling.readFile("words_alpha.txt")
        if(resultset == False):
            raise Exception("Cannot open dictionary file. Ending Program")
            sys.exit(1)     #problem
        else:
            dictionaryTuple = (resultset.split())
            return dictionaryTuple[random.randrange(0,len(dictionaryTuple) - 1)]
    elif(wordType == 2):
        y = WordCategory.DifferentCat()
        return y.colorWordCreator()[random.randrange(0,len(y.colorWordCreator()) - 1)]
    elif(wordType == 3):
        y = WordCategory.DifferentCat()
        return y.carManuCreator()[random.randrange(0,len(y.carManuCreator()) - 1)]
#Infinite loop
keepRunning = True
userChoice  = True
while keepRunning:
    #User's number of guesses
    userGuess = 0
    #Create hangman picture
    myList = HangMe.Hangman.hangStage()
    #Figure out which game user wants to play
    while userChoice:
        userInput = int(input("Do you want to guess (1)unlimited, (2)colors, (3)car manufacturers: "))
        if((userInput > 0) and (userInput <= 3)):
            if (userInput == 1):
                wordOfTheDay = createRandomWord(1)
            elif (userInput == 2):
                wordOfTheDay = createRandomWord(2)
            elif (userInput == 3):
                wordOfTheDay = createRandomWord(3)
            break
    #Instantiate Game engine class
    x = Processor()
    x.secretWord = wordOfTheDay
    x.createAsterix()
    #print(x.secretWord)
    x.list2String()
    print(x.progressWord)
    while userGuess < 8:
        if(x.findCharOccurence(input("Enter a letter: "))):
            x.list2String()
            print(myList[userGuess])
            print(x.progressWord)
        else:
            userGuess += 1
            x.list2String()
            print(myList[userGuess])
            print(x.progressWord)
        if(userGuess == 8) and (x.progressWord != x.secretWord):
            print("You lost!")
            break
        elif(userGuess < 8) and (x.progressWord == x.secretWord):
            print("Congrats!")
            break
    print("Word is \"" + x.secretWord + "\"")
    #How to exit game
    if(input("Do you want to play again? y or n ") != "y"):
        keepRunning = False

sys.exit(0)

