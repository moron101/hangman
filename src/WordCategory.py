class DifferentCat():
    def __init__(self):     #gotta remember constructor is like this..
        self.myDict = {}
    def colorWordCreator(self):
        self.myDict["colors"] = ("red", "orange","yellow","green", "blue", "indigo","violet")
        return self.myDict["colors"]
    def carManuCreator(self):
        self.myDict["carManufacturers"] = ("fiat", "chrysler", "jeep", "ram","honda", "acura", "ford","lincoln","hyundai","kia", "genesis","chevrolet","gmc","cadillac","buick","nissan","infiniti","toyota","lexus","volkswagen","audi","lamborghini","ferrari","alfaromeo","maserati","ferrari","mclaren","mercedes","bmw","mini","landrover","jaguar","rollsroyce","bentley","bugatti","mazda","subaru")
        return self.myDict["carManufacturers"]