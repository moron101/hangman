import os

class FileHandling():
    #pass    # use the pass keyword when you do not want to add any properties or methods to the class

    def write2File(fileName, string2Write):
        try:
            myFile = open(fileName,"a")
            myFile.write("\n" + string2Write + "\n")
            myFile.close()
            return True
        except:
            print("Problem writing to file")
            return False

    def readFile(fileName):
        try:
            myFile = open(fileName, "r")
            return myFile.read()
        except:
            print("Problem reading file")
            return False

    def createFile(fileName):
        try:
            myFile = open(fileName, "x")
            print("File created")
            return True
        except:
            print("Problem creating file")
            return False

    def deleteFile(fileName):
        try:
            os.remove(fileName)
            print("File deleted")
            return  True
        except:
            print("Problem deleting file")
            return False

    def check4File(fileName):
        if os.path.exists(fileName):
            return True  #success
        else:
            return False  #failure